const { Books } = require('../../models')
const userService = require("../services/books");
const imagekit = require('../../lib/imageKit')

const createBooks = async(req, res) => {
    const { judul, penulis, tahun, warehouseId } = req.body
        // req.body.name, req.body.price, req.body.quantity
    try {
        // // untuk dapat extension file nya
        // const split = req.file.originalname.split('.')
        // const ext = split[split.length - 1]

        // // upload file ke imagekit
        // const img = await imagekit.upload({
        //     file: req.file.buffer, //required
        //     fileName: `${req.file.originalname}.${ext}`, //required
        // })
        // console.log(img.url)

        const newBooks = await userService.createBook(judul, penulis, tahun, warehouseId)

        res.status(201).json({
            status: 'success',
            data: {
                newBooks
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findBooks = async(req, res) => {
    const books = await userService.getAllBooks()
    try {
        res.status(200).json({
            status: 'Success',
            data: {
                books
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}


const findBooksById = async(req, res) => {
    try {
        const books = await userService.getBooksById(req.params.id)
        res.status(200).json({
            status: 'Success',
            data: {
                books
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const updatedBooks = async(req, res) => {
    const { judul, penulis, tahun, warehouseId } = req.body
    const id = req.params.id
    try {
        const books = await userService.updatedBooks(judul, penulis, tahun, warehouseId, id)
        res.status(200).json({
            status: 'Success',
            data: {
                books
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const deleteBooks = async(req, res) => {
    const id = req.params.id
    try {
        await userService.deleteWarehouse(id)
        res.status(200).json({
            status: 'success',
            message: `Berhasil terhapus  `
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

module.exports = {
    createBooks,
    findBooks,
    updatedBooks,
    findBooksById,
    deleteBooks

}