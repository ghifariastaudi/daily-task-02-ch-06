const userService = require("../services/warehouses");
const imagekit = require('../../lib/imageKit');
const { use } = require("../../routes");

const createWarehouses = async(req, res) => {
    const { nama, alamat, pemilik_toko } = req.body

    try {
        const newWarehouses = await userService.createWarehouses(nama, alamat, pemilik_toko)

        res.status(201).json({
            status: 'success',
            data: {
                newWarehouses
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findWarehouses = async(req, res) => {
    const warehouses = await userService.getAllWarehouses()
    try {
        res.status(200).json({
            status: 'Success',
            data: {
                warehouses
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findWarehousesById = async(req, res) => {
    try {
        const warehouses = await userService.getWarehousesById(req.params.id)
        res.status(200).json({
            status: 'Success',
            data: {
                warehouses
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const updatedWarehouses = async(req, res) => {
    const { nama, alamat, pemilik_toko } = req.body
    const id = req.params.id
    try {
        const warehouses = await userService.updatedWarehouses(nama, alamat, pemilik_toko, id)
        res.status(200).json({
            status: 'Success',
            data: {
                warehouses
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const deleteWarehouses = async(req, res) => {
    const id = req.params.id
    try {
        await userService.deleteWarehouse(id)
        res.status(200).json({
            status: 'success',
            message: `Berhasil terhapus  `
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

module.exports = {
    createWarehouses,
    findWarehouses,
    findWarehousesById,
    updatedWarehouses,
    deleteWarehouses

}