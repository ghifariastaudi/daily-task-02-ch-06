const { Warehouses, Books } = require("../../models")

module.exports = {
    getAllBooks() {
        const posts = Books.findAll()
        return posts
    },
    createBook(judul, penulis, tahun, warehouseId) {
        const book = Books.create({
            judul,
            penulis,
            tahun,
            warehouseId
        });
        return book;
    },
    getBooksById(IdBooks) {
        const books = Books.findByPk(IdBooks)
        return books
    },
    updatedBooks(judul, penulis, tahun, warehouseId, id) {
        const books = Books.update({
            judul,
            penulis,
            tahun,
            warehouseId
        }, {
            where: {
                id
            }
        })
        return books
    },
    deleteBooks(id) {
        const books = Books.destroy({
            where: {
                id
            }
        })
        return books
    }
}