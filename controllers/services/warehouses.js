const { Warehouses, Books } = require("../../models")

module.exports = {
    getAllWarehouses() {
        const posts = Warehouses.findAll()
        return posts
    },
    createWarehouses(nama, alamat, pemilik_toko) {
        const warehouses = Warehouses.create({
            nama,
            alamat,
            pemilik_toko
        })
        return warehouses
    },
    getWarehousesById(IdWarehouses) {
        const warehouses = Warehouses.findByPk(IdWarehouses)
        return warehouses
    },
    updatedWarehouses(nama, alamat, pemilik_toko, id) {
        const warehouses = Warehouses.update({
            nama,
            alamat,
            pemilik_toko
        }, {
            where: {
                id
            }
        })
        return warehouses
    },
    deleteWarehouse(id) {
        const warehouses = Warehouses.destroy({
            where: {
                id
            }
        })
        return warehouses
    }

}