const router = require('express').Router()
    // Controller
const Books = require('../controllers/api/booksController')
const Warehouses = require('../controllers/api/warehouseController')

// middleware
const uploader = require('../middlewares/uploader')

// API server
router.post('/api/books', Books.createBooks)
router.get('/api/books', Books.findBooks)
router.get('/api/books/:id', Books.findBooksById)
router.post('/api/books/:id', Books.updatedBooks)
router.delete('/api/books/:id', Books.deleteBooks)

router.post('/api/warehouses', Warehouses.createWarehouses)
router.get('/api/warehouses', Warehouses.findWarehouses)
router.get('/api/warehouses/:id', Warehouses.findWarehousesById)
router.post('/api/warehouses/:id', Warehouses.updatedWarehouses)
router.delete('/api/warehouses/:id', Warehouses.deleteWarehouses)

module.exports = router