'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Warehouses extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    Warehouses.init({
        nama: DataTypes.STRING,
        alamat: DataTypes.STRING,
        pemilik_toko: DataTypes.STRING
    }, {
        sequelize,
        modelName: 'Warehouses',
    });
    Warehouses.associate = function(models) {
        //association can be defined here
        Warehouses.hasMany(models.Books, {
            foreignKey: 'warehouseId'
        })
    }

    return Warehouses;
};