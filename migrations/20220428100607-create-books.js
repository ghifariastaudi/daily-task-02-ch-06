'use strict';
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Books', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            judul: {
                type: Sequelize.STRING,
                allowNull: false
            },
            penulis: {
                type: Sequelize.STRING,
                allowNull: false
            },
            tahun: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            foto: {
                type: Sequelize.STRING
            },
            warehouseId: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Books');
    }
};